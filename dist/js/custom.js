// JavaScript Document
/*********************/
//<![CDATA[
    $(window).load(function() { // makes sure the whole site is loaded
        $('.sonar-wrapper').fadeOut(); // will first fade out the loading animation
        $('#loadert').delay(50).fadeOut('slow'); // will fade out the white DIV that covers the website.
        $('body').delay(50).css({'overflow':'visible'});
    })
//]]>
//endLoader Animation 

//Check to see if the window is top if not then display button
$(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
        $('.scrollToTop').fadeIn();
    } else {
        $('.scrollToTop').fadeOut();
    }
    });	
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},700);
    return false;
});		
//endScrollToTop

function openNav() {
    document.getElementById("mySidepUp").style.height = "auto";
    $('#mySidepUp').hide().delay(300).fadeIn(400);
    setTimeout(function(){
        $("#mySidepUp").fadeIn(400);
    }, 300)
}	
function closeNav() {
    document.getElementById("mySidepUp").style.height = "0";
}
// end Chat Popup

function openNotify() {
	document.getElementById("noticeBar").style.width = "280px";
}	
function closeNotify() {
	document.getElementById("noticeBar").style.width = "0";
}
//end Notification Side bar

$(document).ready(function() {    
    //on hover Menu 
    $('.head_login .dropdown').hover(function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(0).fadeIn(0);
    },
    function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(0).fadeOut(0);
    });	

    //Sticky Menu
    jQuery('#nav_bg').stickit({scope: StickScope.Document, zIndex: 101}); 
    jQuery('#nav_media').stickit({scope: StickScope.Document, zIndex: 101});    

    //Side Menu
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });
    $('#dismiss, .overlay').on('click', function () {
        $("body").removeClass("nav_open");
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');        
    });
    $('#sidebarCollapse').on('click', function () {
        $("body").addClass("nav_open");
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    }); 
    
    //Same Reader Slider		
    var owl = $('#readerOn_slider');
    owl.owlCarousel({
        items: 4,
        loop: true,
        margin: 30,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause:true,
        navRewind: true,
        dots: false,
        nav: false,
        navText : ["&#xe875","&#xe876"],
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            568: {
                items: 2
            },
            600: {
                items: 2
            },
            768: {
                items: 3
            },
            1024: {
                items: 4 
            }
        }
    })
	var block = false;
	jQuery(".owl-item").mouseenter(function() {
		if (!block) {
			block = true;
			owl.trigger('stop.owl.autoplay')
			block = false;
		}
	});
	jQuery(".owl-item").mouseleave(function() {
		if (!block) {
			block = true;
			owl.trigger('play.owl.autoplay', [3000])
			block = false;
		}
	});	
    //end owl
    //Parallax
    $('#common_banner').parallax("10%", 0.3);     
    $('.aboutCont_bnr').parallax("10%", 0.3); 
    $('.research_sec').parallax("50%", 0.3);
    //Animation 
    new WOW().init();
    //end all Animation

    // Nav Brand Filter
    var filterList = {		
        init: function () {
            jQuery('#portfoliolist').mixItUp({
                selectors: {
                    target: '.portfolio',
                    filter: '.filter'	
                },
                load: {  
                    filter: '.a'  // if show the First view on particular category
                }     
            });								
        
        }
    };    
    filterList.init();
    // end Filter

    $(function () {
        $(".testiBox").slice(0, 8).show();
        $("#loadMore").on('click', function (e) {
            e.preventDefault();
            $(".testiBox:hidden").slice(0, 3).slideDown();
            if ($(".testiBox:hidden").length == 0) {
                $("#load").fadeOut('slow');
            }
            $('html,body').animate({
                scrollTop: $(this).offset().top
            }, 1500);
        });
    });
    // endLoad More    
    

})




//Manage Document
function readProImg(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();                
        reader.onload = function (e) {
            var imgurl= "url('"+e.target.result+"')";
            $('#profImg_upl').css('background-image', imgurl);
        }                
        reader.readAsDataURL(input.files[0]);
    }
} 
$("#profImg_Inp").change(function(){
    readProImg(this);
});
//Manage Document 2
function readProImg2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();                
        reader.onload = function (e) {
            var imgurl= "url('"+e.target.result+"')";
            $('#profImg_upl2').css('background-image', imgurl);
        }                
        reader.readAsDataURL(input.files[0]);
    }
} 
$("#profImg_Inp2").change(function(){
    readProImg2(this);
});



	
	
	
